<?php

namespace common\models;    
use yii\web\UploadedFile;       
use Yii;
// use yii\helpers\Url;
/**
 * This is the model class for table "banner".
 *
 * @property int $id
 * @property string $title
 * @property string $image
 * @property int $status
 * @property int $type 1=photo, 2=video
 * @property string $youtube_id
 */
class Banner extends \yii\db\ActiveRecord     
{   
    /**
     * {@inheritdoc}
     */
    public static function tableName()     
    {
        return 'banner';       
    }     
    
    static $type = [
        1 => 'Photo',  
        2 => 'Video'
    ];

    static $status = [  
        0 => 'Inactive',
        1 => 'Active'
    ];

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'type','status'], 'required'], 
            [[ 'type'], 'integer'],
            [['title'], 'string', 'max' => 200],
            [['image', 'youtube_id'], 'string', 'max' => 255],  

            ['image','required', 'when' => function($model) { 
                return $model->type == 1;
            }, 'enableClientValidation' => false],
            ['youtube_id','required', 'when' => function($model) {  
                return $model->type == 2;  
            }, 'enableClientValidation' => false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()     
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image' => 'Image',
            'status' => 'Status',
            'type' => 'Type',
            'youtube_id' => 'Youtube ID',     
        ];
    }

    public function saveFile()  
    { 
        $instance = UploadedFile::getInstance($this, 'image');
        if ($instance) {
            $image_name = $instance->baseName . '.' . $instance->extension;   
                $instance->saveAs('uploads/banner/' . $image_name);
               return $this->image = $image_name;

        }else{
			if(!empty($this->oldAttributes)) return $this->image = $this->oldAttributes['image'];
		}
        return true;
    }    
}
