<?php

namespace common\models;
use yii\web\UploadedFile;
use Yii;  
use yii\behaviors\SluggableBehavior;    
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "event".
 *
 * @property int $id
 * @property string $title
 * @property string $short_description
 * @property string $description
 * @property string $slug
 * @property int $image
 * @property int $status
 * @property int $created_by
 * @property int $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property int $category_id
 * @property string $event_date
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event';
    }

    static $status = [
        0 => 'Inactive',
        1 => 'Active'
    ];

    public function behaviors()
    {
            return  [
                [
                'class' => SluggableBehavior::className(),  
                'attribute' => 'title',
                // 'slugAttribute' => 'slug',
                ],
                [
                    'class' => TimestampBehavior::className(),  
                    'createdAtAttribute' => 'created_at',  
                    'updatedAtAttribute' => 'updated_at',   
                    'value' => new Expression('NOW()'),
                ], 
            ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'short_description', 'description','image', 'category_id', 'event_date'], 'required'],
            [['short_description', 'description'], 'string'],
            [[ 'status', 'created_by', 'updated_by', 'category_id'], 'integer'],
            [['created_at', 'updated_at', 'event_date'], 'safe'],
            [['image','title'], 'string', 'max' => 200],
            [['slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'slug' => 'Slug',
            'image' => 'Image',
            'status' => 'Status',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'category_id' => 'Category ID',
            'event_date' => 'Event Date',
        ];
    }

    public function saveFile()
    { 
        // $model = new TravelAgency();
        $instance = UploadedFile::getInstance($this, 'image');
        if ($instance) {
            $image_name = $instance->baseName . '.' . $instance->extension;                          
                $instance->saveAs('uploads/event/' . $image_name);
               return $this->image = $image_name;

        }else{
			if(!empty($this->oldAttributes)) return $this->image = $this->oldAttributes['image'];
		}
        return true;
    }    

    public function getUser()      
    {
       return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdatedby()  
    {
       return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function createdBy(){
        return $this->created_by=yii::$app->user->id;    
        $this->save();  
     }
 
     public function updatedBy(){    
         return  $this->updated_by=yii::$app->user->id;
        $this->save();  
         
     }

    public function getThumbImage(){
        return Yii::$app->urlManagerBackend->baseUrl.'/event/'.$this->image;
    }

    public function getBigImage(){
        return Yii::$app->urlManagerBackend->baseUrl.'/event/'.$this->image;
    }

    public function getEventDate(){
        return date('M d, Y', strtotime($this->event_date));
    }
}
