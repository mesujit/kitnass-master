<?php

namespace common\models;
use yii\web\UploadedFile;
use yii\behaviors\SluggableBehavior;    
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use Yii;      

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $title
 * @property string $short_description
 * @property string $description
 * @property string $slug
 * @property int $image
 * @property int $status
 * @property int $created_by
 * @property int $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property int $category_id
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    static $status = [    
        0 => 'Inactive',
        1 => 'Active'
    ];

    /**
     * {@inheritdoc}
     */

    public function behaviors()  
    {
            return  [
                [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                // 'slugAttribute' => 'slug',
                ],
                [
                    'class' => TimestampBehavior::className(),
                    'createdAtAttribute' => 'created_at',
                    'updatedAtAttribute' => 'updated_at',
                    'value' => new Expression('NOW()'),
                ], 
            ];
    }

    public function rules()  
    {
        return [
            [['title', 'short_description', 'description','status', 'category_id'], 'required'],
            [['short_description', 'description'], 'string'],
            [[ 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['image','title'], 'string', 'max' => 200],
            [['slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'slug' => 'Slug',
            'image' => 'Image',
            'status' => 'Status',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'category_id' => 'Category ID',
        ];  
    }

    

    public function saveFile()   
    { 
        // $model = new TravelAgency();
        $instance = UploadedFile::getInstance($this, 'image');  
        if ($instance) {
            $image_name = $instance->baseName . '.' . $instance->extension;                          
                $instance->saveAs('uploads/news/' . $image_name);
               return $this->image = $image_name;

        }else{
            if(!empty($this->oldAttributes)) $this->image = $this->oldAttributes['image'];
            return true;
		}
        return true;
    }    

    public function getUser()            
    {
       return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdatedby()  
    {
       return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }


    
    public function createdBy(){
       return $this->created_by=yii::$app->user->id;    
       $this->save();  
    }

    public function updatedBy(){    
        return  $this->updated_by=yii::$app->user->id;
       $this->save();  
        
    }

    public static function getNews($category, $limit, $orderBy){
        return self::find()->where(['status' => 1, 'category_id' => $category])
        ->orderBy("$orderBy desc")->limit($limit)->all();
    }

    public static function getBigImage($news){
        return Yii::$app->urlManagerBackend->baseUrl.'/news/'.$news['image'];
    }

    public function getThumbImage(){
        return Yii::$app->urlManagerBackend->baseUrl.'/news/'.$this->image;
    }

    public function categoryText(){
        $category = "";
        foreach($this->category_id as $cat){
            $category .= "|".$cat."|";
        }
        $this->category_id = $category;
        return true;
    }

    public function getCategoryText(){
        $explode = explode("|", $this->category_id);
        
        $category = [];
        foreach($explode as $cat){
            if(!empty($cat))
            $category[$cat] = $cat;
        }
        return $category;
        
        
    }

    
}
