<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "award".
 *
 * @property int $id
 * @property int $celebrity_id
 * @property string $name
 * @property string $release_date
 */
class Award extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'award';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'release_date'], 'required'],
            [['celebrity_id'], 'integer'],
            [['release_date'], 'string'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'celebrity_id' => 'Celebrity ID',
            'name' => 'Name',
            'release_date' => 'Release Date',
        ];
    }
}
