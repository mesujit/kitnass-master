<?php

namespace common\models;
use yii\web\UploadedFile;     
use Yii;
use yii\behaviors\SluggableBehavior;    
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use common\models\Award;
use common\models\Filmography;
/**
 * This is the model class for table "celebrity".
 *
 * @property int $id
 * @property string $name
 * @property string $about
 * @property string $title  
 * @property int $type 1=artist, 2=model
 * @property string $filmography
 * @property string $awards
 * @property int $status 0=no, 1=yes
 * @property string $slug
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 */
class Celebrity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'celebrity';
    }
    
    static $type = [
        1 => 'Artist',
        2 => 'Model'
    ];
    static $status = [
        0 => 'Inactive',
        1 => 'Active'
    ];
    /**
     * {@inheritdoc}
     */

    public function behaviors()   
    {
            return  [
                [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                // 'slugAttribute' => 'slug',
                ],
                [
                    'class' => TimestampBehavior::className(),   
                    'createdAtAttribute' => 'created_at',
                    'updatedAtAttribute' => 'updated_at',
                    'value' =>  new Expression('NOW()'),     
                ], 
            ];
    } 

    public function rules()     
    {
        return [
            [['name', 'about', 'title', 'type'], 'required'],
            [['about', 'filmography', 'awards'], 'string'],
            [['type', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'title', 'slug', 'image'], 'string', 'max' => 200],
        ];
    }    

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'about' => 'About',
            'title' => 'Title',
            'type' => 'Type',
            'filmography' => 'Filmography',   
            'awards' => 'Awards',
            'status' => 'Status',
            'slug' => 'Slug',
            'image' => 'Image',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];  
    }


    public function saveFile()    
    { 
        // $model = new TravelAgency();
        $instance = UploadedFile::getInstance($this, 'image');      
        if ($instance) {
            $image_name = $instance->baseName . '.' . $instance->extension;                          
                $instance->saveAs('uploads/celebrity/' . $image_name);
               return $this->image = $image_name;

        }else{
			if(!empty($this->oldAttributes)) return $this->image = $this->oldAttributes['image'];
		}
        return true;  
    }    

    public function getAward()            
    {
       return $this->hasOne(Award::className(), ['celebrity_id' => 'id']);
    }
    public function getFilmography()            
    {
       return $this->hasOne(Filmography::className(), ['celebrity_id' => 'id']);
    }
}
