<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "filmography".
 *
 * @property int $id
 * @property int $celebrity_id
 * @property string $name
 * @property string $release_date
 */
class Filmography extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()  
    {
        return 'filmography';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'release_date'], 'required'],
            [['celebrity_id'], 'integer'],
            [['release_date'], 'safe'],
            [['name'], 'string', 'max' => 65],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'celebrity_id' => 'Celebrity ID',
            'name' => 'Name',
            'release_date' => 'Release Date',
        ];
    }
}
