<?php

namespace common\models;
use yii\web\UploadedFile;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use Yii;

/**
 * This is the model class for table "gallery".
 *
 * @property int $id
 * @property string $title
 * @property string $image
 * @property int $type 1=photo, 2=video
 * @property int $status 0=no, 1=yes
 * @property string $created_at
 * @property string $updated_at
 * @property string $youtube_id
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()  
    {
        return 'gallery';
    }

    static $type = [  
        1 => 'Photo',
        2 => 'Video'
    ];
  
    static $status = [
        0 => 'Inactive',
        1 => 'Active'
    ];
    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
            return  [
                [
                    'class' => TimestampBehavior::className(),         
                    'createdAtAttribute' => 'created_at',  
                    'updatedAtAttribute' => 'updated_at',
                    'value' => new Expression('NOW()'),     
                ], 
            ];
    }
    public function rules()
    {
        return [      
            [['title', 'type', 'status'], 'required'],           
            [['type'], 'integer'],
            [['created_at', 'updated_at','image'], 'safe'],
            [['title', 'youtube_id'], 'string', 'max' => 200],               
            // [['image'], 'string', 'max' => 255],

            ['image','required', 'when' => function($model) { 
                return $model->type == '1';
            }, 'enableClientValidation' => false],
            ['youtube_id','required', 'when' => function($model) { 
                return $model->type == '2';
            }, 'enableClientValidation' => false],
        ];   
    } 

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',   
            'title' => 'Title',
            'image' => 'Image',
            'type' => 'Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'youtube_id' => 'Youtube ID',
        ];
    }
 
    public function saveFile()                  
    { 
        // $model = new TravelAgency();
        $instances = UploadedFile::getInstances($this, 'image');  
        if ($instances) {
            foreach ($instances as $instance){
           
            $image_name = $instance->baseName . '.' . $instance->extension;                          
                $instance->saveAs('uploads/gallery/' . $image_name);
             
                    $save_image[]=$image_name;
                    $this->image = json_encode($save_image);      
                }
        }else{
			if(!empty($this->oldAttributes)) return $this->image = $this->oldAttributes['image'];
		}
        return true;
    }    
}
