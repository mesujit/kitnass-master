<?php

namespace common\models;
use yii\web\UploadedFile;
   
use Yii;

/**
 * This is the model class for table "celebrity_gallery".
 *
 * @property int $id
 * @property int $celebrity_id
 * @property string $image
 * @property int $type 1=photo, 2=video
 * @property string $youtube_id
 */
class CelebrityGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'celebrity_gallery';
    }
    
    static $type = [
        1 => 'Photo',
        2 => 'Video'
    ];

    /**
     * {@inheritdoc}
     */
    public function rules()      
    {     
        return [
            [['name', 'youtube_id'], 'required'],
            [['id', 'celebrity_id', 'type'], 'integer'],
            [['image'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 100],
            [['youtube_id'], 'string', 'max' => 200],

            ['image','required', 'when' => function($model) { 
                return $model->type == '1';
            }],
            ['youtube_id','required', 'when' => function($model) { 
                return $model->type == '2';
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()  
    {
        return [
            'id' => 'ID',
            'celebrity_id' => 'Celebrity ID',
            'image' => 'Image',
            'type' => 'Type',
            'youtube_id' => 'Youtube ID',
        ];
    }  

    public function saveFile()
    { 
        $instance = UploadedFile::getInstance($this, 'image');
        if ($instance) {
            $image_name = $instance->baseName . '.' . $instance->extension;                          
                $instance->saveAs('uploads/celebrity_gallery/' . $image_name);
               return $this->image = $image_name;

        }else{
			if(!empty($this->oldAttributes)) return $this->image = $this->oldAttributes['image'];
		}
        return true;
    }    
}
