<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700',
        'https://fonts.googleapis.com/css?family=Fjalla+One',
        'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900',
        "css/style.css",
        "https://fonts.googleapis.com/css?family=Lato:400,700",
        "css/css-demo-page.css",
        "css/grt-youtube-popup.css",
        "https://use.fontawesome.com/releases/v5.3.1/css/all.css",
        "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css",
        "unitegallery/themes/default/ug-theme-default.css",
        "unitegallery/css/unite-gallery.css",
        "css/nav.css",
    ];
    public $js = [
        "unitegallery/js/jquery-11.0.min.js",
        "unitegallery/js/ug-common-libraries.js",
        "unitegallery/js/ug-functions.js",
        "unitegallery/js/ug-thumbsgeneral.js",
        "unitegallery/js/ug-thumbsstrip.js",
        "unitegallery/js/ug-touchthumbs.js",
        "unitegallery/js/ug-panelsbase.js",
        "unitegallery/js/ug-strippanel.js",
        "unitegallery/js/ug-gridpanel.js",
        "unitegallery/js/ug-thumbsgrid.js",
        "unitegallery/js/ug-tiles.js",
        "unitegallery/js/ug-tiledesign.js",
        "unitegallery/js/ug-avia.js",
        "unitegallery/js/ug-slider.js",
        "unitegallery/js/ug-sliderassets.js",
        "unitegallery/js/ug-touchslider.js",
        "unitegallery/js/ug-zoomslider.js",
        "unitegallery/js/ug-video.js",
        "unitegallery/js/ug-gallery.js",
        "unitegallery/js/ug-lightbox.js",
        "unitegallery/js/ug-carousel.js",
        "unitegallery/js/ug-api.js",
        "unitegallery/themes/default/ug-theme-default.js",
        "js/nav.js",
        "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js",
        "js/grt-youtube-popup.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
