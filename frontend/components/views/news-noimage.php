<?php
use common\models\News; 
?>
<div class="newsSection">
    <div class="newsContentHeading">
        <span>
        <i class="fas fa-headphones-alt"></i></span> 
        <a href="singleblog.html"><?=$widgetName;?></a>
    </div><!-- End of newsContentHeading -->
    
    <?php $i=1;foreach($data as $news){
        $class = ($i == 1) ? "newsContentsActive" : "newsContents";
        ?>
    <div class="<?=$class;?>">
       
        <div class="newsHeading">
            <a href="singleblog.html">
                <?=$news['title'];?>
            </a>
            </div><!-- End of newsHeading -->
    </div><!-- End of newsContents -->

    <?php $i++;}?>
</div><!-- End of newsSection / What's Hot -->