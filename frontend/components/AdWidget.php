<?php
namespace frontend\components;
use yii;
use yii\base\Widget;
use yii\helpers\Html;


class AdWidget extends Widget
{
    public $widgetName = '';
	public $widgetView = '';
	public $limit = 5;
	public $position = 1;
	
	

    public function init()
    {
        parent::init();
//        if ($this->message === null) {
//            $this->message = 'Hello World';
//        }
    }

    public function run()
    {
        return $this->render($this->widgetView,['widgetName'=>$this->widgetName,'limit'=>$this->limit,'data'=>$this->getData(),'position'=>$this->position]);
    }
	
	public function getData(){
		$sql = "select baner_image,link from add_baner where baner_position=".$this->position ." order by id desc limit ".$this->limit;
		$results = Yii::$app->db->createCommand($sql)->queryAll();
		return $results;
		
	}
}
