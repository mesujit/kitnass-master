<?php
namespace frontend\components;
use yii;
use yii\base\Widget;
use yii\helpers\Html;


class NewsWidget extends Widget
{
    public $widgetName = '';
	public $widgetView = '';
	public $limit = 3;
	public $categoryId = '';
	public $textLen = 50;
	public $objectModel = '';
	public $contentEleConfig = [];
	public $catSlug = '';
	
	

    public function init()
    {
        parent::init();
//        if ($this->message === null) {
//            $this->message = 'Hello World';
//        }
    }
/*
 * <div class="modal-footer">
        fafaf
    </div>
 */
    public function run()
    {
        return $this->render($this->widgetView,['widgetName'=>$this->widgetName,'limit'=>$this->limit,'categoryId'=>$this->categoryId,'textLen'=>$this->textLen,'data'=>$this->getData(),'objectModel'=>$this->objectModel,'contentEleConfig'=>$this->contentEleConfig,'catSlug'=>$this->catSlug]);
    }
	
	public function getData(){
		if(!$this->objectModel){
            
			$sql = "select id,title,image,short_description,created_at from news where category_id like '%|".$this->categoryId ."|%' order by id desc limit ".$this->limit;
			//$sql = "select id,title,image,short_description,created_at from news where category_id = $this->categoryId order by id desc limit ".$this->limit;
			$results = Yii::$app->db->createCommand($sql)->queryAll();
			return $results;
		}else return $this->objectModel;
	}
}
