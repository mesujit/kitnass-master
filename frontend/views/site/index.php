<div class="container">
    	<div class="row">
        	<div class="col-md-12">
                <div class="row">
                <div class="col-md-8">
                        <?=$this->render("/home/banner", [
                            'banner' => $banner
                        ])?>
                    
                </div><!-- End of col-md-8 -->
                
                <div class="col-md-4">
                    <?=$this->render("/home/banner_right", [
                        'event' => $event
                    ])?>
                </div><!-- End of col-md-4 -->
            </div><!-- End of row -->
                            
            </div><!-- End of col-md-12 -->
            
            <div class="col-md-12">
            	<div class="adSection">
                	<a href="#"><img class="img-fluid" src="images/ad/bacardi-black.jpg" alt="Bacardi Black" title="Bacardi Black" /></a>
                </div><!-- End of adSection -->
            </div><!-- End of col-md-12 -->
            
            <div class="col-md-12">
            	<?=$this->render("/home/hot_latest_news", [
                    'latestEvent' => $latestEvent,
                ])?>
            </div><!-- End of col-md-12 -->
            
            <div class="col-md-12">
            	<div class="adSection">
                	<a href="#"><img class="img-fluid" src="images/ad/shivamcement.gif" alt="Shivam Cement" title="Shivam Cement" /></a>
                </div><!-- End of adSection -->
            </div><!-- End of col-md-12 -->
            
            <div class="col-md-12">
            	<?=$this->render("/home/movies")?>
            </div><!-- End of col-md-12 -->
            
            <div class="col-md-12">
            	<div class="adSection">
                	<a href="#"><img class="img-fluid" src="images/ad/oppo-f5.jpg" alt="OPPO F5" title="OPPO F5" /></a>
                </div><!-- End of adSection -->
            </div><!-- End of col-md-12 -->
            
            <div class="col-md-12">
            	<?=$this->render("/home/bollywood")?>
            </div><!-- End of col-md-12 -->
            
            <div class="col-md-12">
            	<div class="adSection">
                	<a href="#"><img class="img-fluid" src="images/ad/datsun.jpg" alt="DATSUN Redi GO 1.0L" title="DATSUN Redi GO 1.0L" /></a>
                </div><!-- End of adSection -->
            </div><!-- End of col-md-12 -->
            
            <div class="col-md-12">
            	<?=$this->render("/home/artist")?>
            </div><!-- End of col-md-12 -->
            
            <div class="col-md-12">
            	<div class="adSection">
                	<a href="#"><img class="img-fluid" src="https://www.onlinekhabar.com/ads/maruti-may-1140x100pix.gif" alt="Maruti Cement" title="Maruti Cement" /></a>
                </div><!-- End of adSection -->
            </div><!-- End of col-md-12 -->
            
            <div class="col-md-12">
            	<?=$this->render("/home/model")?>
            </div><!-- End of col-md-12 -->
        </div><!-- End of row -->
    </div><!-- End of container -->