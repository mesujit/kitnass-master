<?php 
use frontend\components\NewsWidget;
?>
<div class="row">
    <div class="col-md-4">
        <?php echo NewsWidget::widget([
            'widgetName'=>"What's Hot",
            'widgetView'=>'news',
            'contentEleConfig'=>['textlen1'=>450,'textlen2'=>150],
            'textLen'=>150,
            'categoryId'=>2,
        ]);?>
                            	
    </div><!-- End of col-md-4 -->
    
    <div class="col-md-4">
        <div class="adContetRight">
            <a href="#"><img class="img-fluid" src="images/ad/merryland-college.jpg" alt="Merryland College" title="Merryland College" /></a>
        </div><!-- End of adContetRight -->
    </div><!-- End of col-md-4 -->
    
    <div class="col-md-4">
        <div class="newsSection">
            <div class="newsContentHeading"><span><i class="fas fa-headphones-alt"></i></span> <a href="singleblog.html">Latest Events</a></div><!-- End of newsContentHeading -->
            
            <?php $i=1;foreach($latestEvent as $data){
                $class = ($i == 1) ? "newsContentsActive" : "newsContents";?>
            <div class="<?=$class;?>">
                <?php if($i == 1){?>
                    <a href="singleblog.html">
                        <img class="img-fluid" src="<?=$data->bigImage;?>" alt="" title="" />
                    </a>
                <?php }?>
                <div class="newsHeading">
                    <a href="singleblog.html">
                        <?=$data->title;?>
                    </a>
                    </div><!-- End of newsHeading -->
            </div><!-- End of newsContents -->

            <?php $i++;}?>
                    
            
        </div><!-- End of newsSection /Latest Events -->
    </div><!-- End of col-md-4 -->
</div><!-- End of row -->