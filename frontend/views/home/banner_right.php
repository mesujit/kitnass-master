

<div class="latestNewsBlog">
    <ul>
        <?php foreach($event as $data){?>
        <li>
            <a href="singleblog.html">
                <?=$data->title;?>
            </a>
            <div class="blogImg">
                <a href="singleblog.html">
                    <img src="<?=$data->thumbImage;?>" alt="" title="" />
                </a>
            </div><!-- End of blogImg -->
            <div class="blogDateTime">
                <?=$data->eventDate;?>
            </div><!-- End of blogDateTime -->
            <p><?=$data->short_description;?></p>
        </li>
        <?php }?>
        
    </ul>
</div><!-- End of latestNewsBlog -->