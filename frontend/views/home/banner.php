<?php 
use yii\helpers\Url;
$baseUrl = Yii::$app->urlManagerBackend->baseUrl;
?>
<div class="bannerSection">
    <div id="gallery" style="display:none;">
        <?php foreach($banner as $data){?>
            <?php if($data->type == 1){
                $image = $baseUrl.'/banner/'.$data->image;
                ?>
                <img alt="<?=$data->title?>" src="<?=$image?>" data-image="<?=$image?>" data-description="<?=$data->title?>">
            <?php }?>
        
            <?php if($data->type == 2){?>   
                <img alt="<?=$data->title?>" data-type="youtube" data-videoid="<?=$data->youtube_id?>" data-description="<?=$data->title?>">
            <?php }?>
        <?php }?>    
        
    </div>
</div><!-- End of bannerSection -->