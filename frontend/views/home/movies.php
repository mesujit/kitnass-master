<?php 
use frontend\components\NewsWidget;
?>
<div class="row">                    
    <div class="col-md-4">
        <?php echo NewsWidget::widget([
            'widgetName'=>"Latest Nepali Movies",
            'widgetView'=>'news',
            'contentEleConfig'=>['textlen1'=>450,'textlen2'=>150],
            'textLen'=>150,
            'categoryId'=>3,
        ]);?>
    </div><!-- End of col-md-4 -->
    
    <div class="col-md-4">
        <?php echo NewsWidget::widget([
            'widgetName'=>"Music on Demand",
            'widgetView'=>'news-noimage',
            'contentEleConfig'=>['textlen1'=>450,'textlen2'=>150],
            'textLen'=>150,
            'limit' => 6,
            'categoryId'=>4,
        ]);?>
        
    </div><!-- End of col-md-4 -->
    
    <div class="col-md-4">
        <?php echo NewsWidget::widget([
            'widgetName'=>"Tourism News",
            'widgetView'=>'news',
            'contentEleConfig'=>['textlen1'=>450,'textlen2'=>150],
            'textLen'=>150,
            'categoryId'=>5,
        ]);?>
        
    </div><!-- End of col-md-4 -->
</div><!-- End of row -->