<div class="sectionHeading">
    <div class="contentHead">
        <h1><span><i class="fas fa-headphones-alt"></i></span> <a href="singleblog.html">Artist Profile</a></h1>
    </div><!-- End of contentHead -->
</div>

<div class="celebritySection">
    <div class="row">
        <div class="col-md-3">
            <a href="celebritysingle.html"><img class="img-fluid" src="images/celebrity/adrinpradhan.jpg" alt="Adrin Pradhan" title="Adrin Pradhan" /></a>
            
            <div class="celebrityDetail">                            
                <div class="celebrityName">Adrin Pradhan</div><!-- End of celebrityName -->
                
                <div class="viewProfile"><a href="celebritysingle.html">ViewProfile</a></div><!-- End of viewProfile -->
            </div><!-- End of celebrityDetail -->
        </div><!-- End of col-md-3 -->
        
        <div class="col-md-3">
            <a href="celebritysingle.html"><img class="img-fluid" src="images/celebrity/sugampokhrel.jpg" alt="Sugam Pokhrel" title="Sugam Pokhrel" /></a>
            
            <div class="celebrityDetail">                            
                <div class="celebrityName">Sugam Pokhrel</div><!-- End of celebrityName -->
                
                <div class="viewProfile"><a href="celebritysingle.html">ViewProfile</a></div><!-- End of viewProfile -->
            </div><!-- End of celebrityDetail -->
        </div><!-- End of col-md-3 -->
        
        <div class="col-md-3">
            <a href="celebritysingle.html"><img class="img-fluid" src="images/celebrity/nabinkbhattarai.jpg" alt="Nabin K. Bhattarai" title="Nabin K. Bhattarai" /></a>
            
            <div class="celebrityDetail">                            
                <div class="celebrityName">Nabin K. Bhattarai</div><!-- End of celebrityName -->
                
                <div class="viewProfile"><a href="celebritysingle.html">ViewProfile</a></div><!-- End of viewProfile -->
            </div><!-- End of celebrityDetail -->
        </div><!-- End of col-md-3 -->
        
        <div class="col-md-3">
            <a href="celebritysingle.html"><img class="img-fluid" src="images/celebrity/girishkhatiwada.jpg" alt="Girish Khatiwada" title="Girish Khatiwada" /></a>
            
            <div class="celebrityDetail">                            
                <div class="celebrityName">Girish Khatiwada</div><!-- End of celebrityName -->
                
                <div class="viewProfile"><a href="celebritysingle.html">ViewProfile</a></div><!-- End of viewProfile -->
            </div><!-- End of celebrityDetail -->
        </div><!-- End of col-md-3 -->
    </div><!-- End of row -->
</div><!-- End of celebritySection -->