<div class="headerSection">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="logo">
                    <a href="index.html"><img class="img-fluid" src="images/logo-main.png" alt="" title="Silver Entertainment" /></a>
                </div><!-- End of logo -->
            </div><!-- End of col-md-3 -->
            
            <div class="col-md-offset-1 col-md-9">
                <div class="headerRight">
                    <img class="img-fluid float-right" src="images/social-plugins-img.png" alt="" title="" />
                </div><!-- End of headerRight -->
            </div><!-- End of col-md-9 -->
        </div><!-- End of row -->
    </div><!-- End of container -->
</div><!-- End of headerSection -->

<div class="mainNav">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                	<div id='cssmenu'>
                        <ul>
                           <li><a href='#'>Home</a></li>
                           <li><a href='#'>About Us</a></li>
                           <li><a href='#'>Events</a></li>
                           <li><a href='#'>Model</a></li>
                           <li class='active'><a href='#'>Gallery</a>
                              <ul>
                                 <li><a href='#'>Photo</a></li>
                                 <li><a href='#'>Video</a></li>
                              </ul>
                           </li>
                           <li><a href='#'>Music</a></li>
                           <li><a href='#'>Bollywood</a></li>
                           <li><a href='#'>Hollywood</a></li>
                           <li class='active'><a href='#'>Artist</a>
                              <ul>
                                 <li><a href='#'>Artist Profile</a></li>
                                 <li><a href='#'>Recent Models</a></li>
                              </ul>
                           </li>
                           <li><a href='#'>Contact Us</a></li>
                        </ul>
                    </div>
                </div><!-- End of col-md-12 -->
            </div><!-- End of row -->
        </div><!-- End of container -->
    </div><!-- End of mainNav -->
    