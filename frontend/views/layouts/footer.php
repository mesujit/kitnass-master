<div class="footerSection">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                	<div class="footerSocialMedia">
                    	<ul>
                        	<li><a href="https://www.facebook.com/silverentertainment" target="_blank">f</a></li>
                        	<li><a href="#">l</a></li>
                        	<li><a href="https://www.youtube.com/channel/UCNF09SqLLInrxgzJMTVRSZQ" target="">x</a></li>
                        	<li><a href="#">h</a></li>
                        	<li><a href="#">g</a></li>
                        </ul>
                    </div><!-- End of footerSocialMedia -->
                    
                    <div class="footerNav">
                    	<ul>
                        	<li><a href="#">Home</a></li>
                            <li><a href="#">News</a></li>
                            <li><a href="#">Video</a></li>
                            <li><a href="#">Music</a></li>
                            <li><a href="#">Gallery</a></li>
                            <li><a href="#">Events</a></li>
                            <li><a href="#">Music</a></li>
                            <li><a href="#">Films</a></li>
                            <li><a href="#">Bollywood</a></li>
                            <li><a href="#">Hollywood</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms of Services</a></li>
                        </ul>
                    </div><!-- End of footerNav -->
                    
                    <div class="footerNotes">
                    	<p>Your source for entertainment news, celebrities, celeb news, and celebrity gossip. Check out the hottest fashion, photos, movies and TV shows!</p>
                    </div><!-- End of footerNotes -->
                    
                    <div class="copyrightSection">
                    	<p>Copyright &copy; 2017/2018 <a href="#">Silver Entertainment Media (P) Ltd.</a> All rights reserved.</p>
                    </div><!-- End of copyrightSection -->
                </div><!-- End of col-md-12 -->
            </div><!-- End of row -->
        </div><!-- End of container -->
    </div><!-- End of footerSection -->