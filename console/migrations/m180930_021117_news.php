<?php

use yii\db\Migration;

/**
 * Class m180930_021117_news
 */
class m180930_021117_news extends Migration
{
    /**
     * {@inheritdoc}
     */
    public $table = "{{%news}}";     
    
    /**
     * {@inheritdoc}    
     */
    public function safeUp()    
    {   
        $sql="CREATE TABLE `news` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `title` varchar(200) NOT NULL,
            `short_description` text NOT NULL,
            `description` text NOT NULL,
            `slug` varchar(255) NOT NULL,
            `image` varchar(200) NOT NULL,
            `status` tinyint(4) NOT NULL DEFAULT '0',
            `created_by` int(11) NOT NULL,
            `updated_by` int(11) NOT NULL,
            `created_at` date NOT NULL,
            `updated_at` date NOT NULL,
            `category_id` int(11) NOT NULL,
            PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

        $this->execute($sql);  
        
    }   

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%news}}');  

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180930_021117_news cannot be reverted.\n";

        return false;
    }
    */
}
