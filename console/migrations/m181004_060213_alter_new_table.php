<?php

use yii\db\Migration;

/**
 * Class m181004_060213_alter_new_table
 */
class m181004_060213_alter_new_table extends Migration
{
    public $table = "{{%news}}";
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE $this->table CHANGE `category_id` `category_id` VARCHAR(255) NOT NULL;");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181004_060213_alter_new_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181004_060213_alter_new_table cannot be reverted.\n";

        return false;
    }
    */
}
