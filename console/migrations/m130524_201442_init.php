<?php

use yii\db\Migration;

class m130524_201442_init extends Migration   
{
    public $table = "{{%user}}";     
    
    /**
     * {@inheritdoc}    
     */
    public function safeUp()   
    {   
        $sql="CREATE TABLE `user` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
            `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `status` smallint(6) NOT NULL DEFAULT '10',
            `created_at` int(11) NOT NULL,
            `updated_at` int(11) NOT NULL,
            `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
            `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
            `mobile` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
            `address` varchar(90) COLLATE utf8_unicode_ci NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY `username` (`username`),
            UNIQUE KEY `email` (`email`),
            UNIQUE KEY `password_reset_token` (`password_reset_token`)
           ) ENGINE=InnoDB DEFAULT CHARSET=latin1";

        $this->execute($sql);  
        
    }   

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');  

    }
}
