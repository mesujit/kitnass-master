<?php

use yii\db\Migration;

/**
 * Class m180930_021059_gallery
 */
class m180930_021059_gallery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public $table = "{{%gallery}}";     
    
    /**
     * {@inheritdoc}    
     */
    public function safeUp()     
    {   
        $sql="CREATE TABLE `gallery` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `title` varchar(200) NOT NULL,
            `image` varchar(255) DEFAULT NULL,
            `type` int(11) NOT NULL COMMENT '1=photo, 2=video',
            `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes',
            `created_at` date NOT NULL,
            `updated_at` date NOT NULL,
            `youtube_id` varchar(200) DEFAULT NULL,
            PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

        $this->execute($sql);  
        
    }   

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%gallery}}');  

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180930_021059_gallery cannot be reverted.\n";

        return false;
    }
    */
}
