<?php

use yii\db\Migration;

/**
 * Class m181001_044006_award
 */
class m181001_044006_award extends Migration
{
    /**
     * {@inheritdoc}
     */
    public $table = "{{%award}}";     
    
    /**
     * {@inheritdoc}    
     */
    public function safeUp()   
    {   
        $sql="CREATE TABLE `award` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `celebrity_id` int(11) NOT NULL,
            `name` varchar(50) NOT NULL,
            `release_date` varchar(13) NOT NULL,
            PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=latin1";

        $this->execute($sql);  
        
    }   

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%award}}');  

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181001_044006_award cannot be reverted.\n";

        return false;
    }
    */
}
