<?php

use yii\db\Migration;

/**
 * Class m180930_021042_event
 */
class m180930_021042_event extends Migration
{
    /**
     * {@inheritdoc}
     */
    public $table = "{{%event}}";       
    
    /**
     * {@inheritdoc}    
     */
    public function safeUp()   
    {   
        $sql="CREATE TABLE `event` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `title` varchar(200) NOT NULL,
            `short_description` text NOT NULL,
            `description` text NOT NULL,
            `slug` varchar(255) NOT NULL,
            `image` varchar(200) NOT NULL,
            `status` tinyint(4) NOT NULL DEFAULT '0',
            `created_by` int(11) NOT NULL,
            `updated_by` int(11) NOT NULL,
            `created_at` date NOT NULL,
            `updated_at` date NOT NULL,
            `category_id` int(11) NOT NULL,
            `event_date` date NOT NULL,
            PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

        $this->execute($sql);  
        
    }   

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%event}}');  

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180930_021042_event cannot be reverted.\n";

        return false;
    }
    */
}
