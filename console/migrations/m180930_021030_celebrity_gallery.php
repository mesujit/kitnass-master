<?php

use yii\db\Migration;

/**
 * Class m180930_021030_celebrity_gallery
 */
class m180930_021030_celebrity_gallery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public $table = "{{%celebrity_gallery}}";     
    
    /**
     * {@inheritdoc}    
     */ 
    public function safeUp()   
    {   
        $sql="CREATE TABLE `celebrity_gallery` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `celebrity_id` int(11) NOT NULL,
            `image` varchar(255) NOT NULL,
            `type` int(11) NOT NULL COMMENT '1=photo, 2=video',
            `youtube_id` varchar(200) DEFAULT NULL,
            `name` varchar(100) NOT NULL,
            PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

        $this->execute($sql);  
        
    }   

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%celebrity_gallery}}');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180930_021030_celebrity_gallery cannot be reverted.\n";

        return false;
    }
    */
}
