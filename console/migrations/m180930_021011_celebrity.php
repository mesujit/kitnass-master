<?php

use yii\db\Migration;

/**
 * Class m180930_021011_celebrity
 */
class m180930_021011_celebrity extends Migration
{
    /**
     * {@inheritdoc}
     */
    public $table = "{{%celebrity}}";     
    
    /**
     * {@inheritdoc}    
     */
    public function safeUp()   
    {   
        $sql="CREATE TABLE `celebrity` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(200) NOT NULL,
            `about` text NOT NULL,
            `title` varchar(200) NOT NULL,
            `type` int(11) NOT NULL COMMENT '1=artist, 2=model',
            `filmography` text NOT NULL,
            `awards` text NOT NULL,
            `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes',
            `slug` varchar(200) NOT NULL,
            `image` varchar(200) NOT NULL,
            `created_at` date NOT NULL,
            `updated_at` date NOT NULL,
            PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8"; 

        $this->execute($sql);  
        
    }   

    /**
     * {@inheritdoc}  
     */
    public function safeDown()
    {
        $this->dropTable('{{%celebrity}}');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180930_021011_celebrity cannot be reverted.\n";

        return false;
    }
    */
}
