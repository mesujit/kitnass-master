<?php

use yii\db\Migration;

/**
 * Class m180930_020942_banner
 */
class m180930_020942_banner extends Migration
{
    /**
     * {@inheritdoc}
     */
    public $table = "{{%banner}}";         
    
    /**
     * {@inheritdoc}    
     */
    public function safeUp()   
    {   
        $sql="CREATE TABLE `banner` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `title` varchar(200) NOT NULL,
            `image` varchar(255) NOT NULL,
            `status` tinyint(4) NOT NULL DEFAULT '0',
            `type` int(11) NOT NULL COMMENT '1=photo, 2=video',
            `youtube_id` varchar(255) NOT NULL,
            PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

        $this->execute($sql);  
        
    }   

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%banner}}');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180930_020942_banner cannot be reverted.\n";

        return false;
    }
    */
}
