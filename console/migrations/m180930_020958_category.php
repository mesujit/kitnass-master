<?php

use yii\db\Migration;

/**
 * Class m180930_020958_category
 */
class m180930_020958_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public $table = "{{%category}}";     
    
    /**
     * {@inheritdoc}    
     */
    public function safeUp()   
    {   
        $sql="CREATE TABLE `category` (
         `id` int(11) NOT NULL AUTO_INCREMENT,
         `title` varchar(200) NOT NULL,
         `type` int(11) NOT NULL COMMENT '1=event, 2=news',
         PRIMARY KEY (`id`),
            `updated_at` int(11) DEFAULT NULL
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

        $this->execute($sql);  
        
    }   

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%category}}');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180930_020958_category cannot be reverted.\n";

        return false;
    }
    */
}
