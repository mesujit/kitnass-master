<?php

use yii\db\Migration;

/**
 * Class m181001_044022_filmography
 */
class m181001_044022_filmography extends Migration
{
    /**
     * {@inheritdoc}
     */
    public $table = "{{%filmography}}";     
    
    /**
     * {@inheritdoc}    
     */
    public function safeUp()   
    {   
        $sql="CREATE TABLE `filmography` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `celebrity_id` int(11) NOT NULL,
            `name` varchar(65) NOT NULL,
            `release_date` varchar(13) NOT NULL,
            PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=latin1";

        $this->execute($sql);  
        
    }   

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%filmography}}');  

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181001_044022_filmography cannot be reverted.\n";

        return false;
    }
    */
}
