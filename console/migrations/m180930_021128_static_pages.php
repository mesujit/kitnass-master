<?php

use yii\db\Migration;

/**
 * Class m180930_021128_static_pages
 */
class m180930_021128_static_pages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public $table = "{{%static_page}}";     
    
    /**
     * {@inheritdoc}    
     */
    public function safeUp()    
    {   
        $sql="CREATE TABLE `static_page` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `title` varchar(100) NOT NULL,
            `short_description` text,
            `description` text NOT NULL,
            `slug` varchar(255) NOT NULL,
            `created_at` date NOT NULL,
            `updated_at` date NOT NULL,
            PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

        $this->execute($sql);  
        
    }   

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%static_page}}');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180930_021128_static_pages cannot be reverted.\n";

        return false;
    }
    */
}
