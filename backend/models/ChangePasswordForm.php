<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;
  
/** 
 * Login form
 */
class ChangePasswordForm extends Model
{
    public $old_password;
    public $new_password;
    public $confirm_password;             
	public $password;     
	public $_user;
    
    /**   
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
			['old_password','required','on'=>'update'],
			['old_password', 'validatePassword', 'on' => 'update' ],
            [['new_password','confirm_password'], 'required'],
			['confirm_password','compare','compareAttribute'=>'new_password','message'=>"Passwords don't match", 'on' => 'update' ],
            // rememberMe must be a boolean value
            // password is validated by validatePassword()
		
        ];
    }   
                        
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->old_password)) {
                $this->addError($attribute, 'Old Password did not match.');
            }
        }
       
    } 
        
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()       
    {    
        if ($this->_user === null) {
            $this->_user = User::findByUsername(Yii::$app->user->identity->username);
        }

        return $this->_user;  
    }
}
    