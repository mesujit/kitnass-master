<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;   
use yii\bootstrap\ActiveForm;
   
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;   
?>    
<div class="site-login">         
    <h1>Login</h1>
    <p>Please fill out the following fields to login:</p>              

      <div class="login-box">
        <div class="login-box-body">   
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>     
     
                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end();?>                      
             <ul>
                <li><?= Html::a("Forgot password?",["site/request-password-reset"])?> </li>
             </ul>         
        </div>                 
    </div>
</div>            
<style>    
    .login-box, .register-box {          
        width: 360px;
        margin: 7% auto;
    }
    .login-box-body, .register-box-body {
        background: #fff;
        padding: 20px;
        border-top: 0;
        color: #666;
    }
    .site-login{
        width: 360px;
        margin: 7% auto;
    }
</style>