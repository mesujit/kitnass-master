<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Forget Password';
$this->params['breadcrumbs'][] = $this->title;   
?>
<div class="forget-password-box">      
    <div class="forget-password-box-body">
        <h1><?= Html::encode($this->title) ?></h1>          

        <p>Please fill out your email. A link to reset password will be sent there.</p>

        <div class="row">
            <div class="col-lg-5">  
                <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
                    </div>
  
                <?php ActiveForm::end(); ?>
            </div>  
        </div>
    </div>
</div>

<style>
    .forget-password-box,
.register-box {
  width: 525px;
  margin: 7% auto;
}


.forget-password-box-body,
.register-box-body {
  background: #fff;
  padding: 20px;
  border-top: 0;
  color: #666;
}

</style>