<?php

/* @var $this yii\web\View */  
use yii\helpers\Html; 
use yii\helpers\Url;
$this->title = 'Dashboard';           
?>        
    <div class="kt-pagetitle">         
      <h5>Dashboard</h5>     
    </div> 
    <div class="kt-pagebody">              
    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body pd-b-0">
                <h6 class="card-body-title tx-12 tx-spacing-2 mg-b-20 tx-success">Users</h6>
                <h2 class="tx-lato tx-inverse"> <?= $total_user ?></h2>
                <!-- <p class="tx-12"><span class="tx-success">2.5%</span> change from yesterday</p> -->
                </div><!-- card-body -->
                <div id="rs1" class="ht-50 ht-sm-70 mg-r--1"></div>
            </div><!-- card -->
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body pd-b-0">
                <h6 class="card-body-title tx-12 tx-spacing-2 mg-b-20 tx-success">Total Celebrity</h6>
                <h2 class="tx-lato tx-inverse"> <?= $total_Celebrity ?></h2>
                <!-- <p class="tx-12"><span class="tx-success">2.5%</span> change from yesterday</p> -->
                </div><!-- card-body -->
                <div id="rs2" class="ht-50 ht-sm-70 mg-r--1 "></div> 
            </div><!-- card -->
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body pd-b-0">
                <h6 class="card-body-title tx-12 tx-spacing-2 mg-b-20 tx-success">Total news today</h6>
                <h2 class="tx-lato tx-inverse"> <?= $total_news_today ?></h2>
                <!-- <p class="tx-12"><span class="tx-success">2.5%</span> change from yesterday</p> -->
                </div><!-- card-body -->
                
                <div id="rs1" class="ht-50 ht-sm-70 mg-r--1 rickshaw_graph"></div>
            </div><!-- card -->
        </div>
        
        <!-- <div class="card card-body"> -->
          <!-- <p class="card-text">Total Users</p> -->
      <?php /* $total_user */?>
          
        <!-- </div> -->
        <!-- card -->
      <!-- <span class="info-box-text">Total Users</span> -->
        

      <? $total_user ?>
    </div>
    <!-- card -->
    <!-- <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">Total Users</span> -->
            <!-- <span class="info-box-number"> -->
              <?php /*$total_user */?>
            <!-- </span>
        </div> -->
        <!-- /.info-box-content -->
        <!-- </div> -->
        <!-- /.info-box -->
    <!-- </div> -->
    
    </div>
    <div class="row">
                <div class="col-md-6">     
                    <div class="box box-info">
                        <div class="box-header with-border">  
                            <h3 class="box-title">Latest events</h3>           
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                            <!-- /.box-header -->
                        <div class="box-body">            
                            <div class="table-responsive">     
                                <table class="table no-margin">                  
                                    <thead>
                                        <tr>
                                            <th>Title</th>     
                                            <th>Image</th>      
                                            <th>Event date</th>
                                        </tr>        
                                    </thead>  
                                    <tbody>
                                        <?php foreach ($latest_events as $latest_events_key => $latest_events_data) {?>
                                                
                                                <td><?= $latest_events_data->title ?></td>
                                                <td><?=  Html::img(Url::base().'/uploads/event/'.$latest_events_data->image,['width' => '60px',
                                                    'height' => '60px'])?></td>
                                                <td><?= $latest_events_data->event_date ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>  
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                                            
                    </div>   
                </div>                           
            </div>
    </div>
<style>
/* .info-box {
    display: block;
    min-height: 90px;
    background: #fff;
    width: 100%;
    box-shadow: 0 1px 1px rgba(0,0,0,0.1);
    border-radius: 2px;
    margin-bottom: 15px;
}

* {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
div {
    display: block;
}
body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
    font-weight: 400;
    overflow-x: hidden;
    overflow-y: auto;
}
body {
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 14px;
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
} */

</style>

