<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php if(Yii::$app->controller->action->id=='changepassword'):?>
 
<?php $form = ActiveForm::begin(['id'=>'change-password']); ?>     
<?php endif;?>        
<div class="cp-form">
	<?= $form->field($changePasswordModel, 'old_password')->passwordInput() ?>  
	<?= $form->field($changePasswordModel, 'new_password')->passwordInput() ?>   
	<?= $form->field($changePasswordModel, 'confirm_password')->passwordInput() ?>
   
	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Change') : Yii::t('app', 'Change'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
		'id'=>'changepassword','data-aurl'=>Yii::$app->urlManager->createUrl(['user/changepassword','id'=>$model->id]),
		'data-formid'=>'changepassword']) ?>
	</div>
</div>
<?php if(Yii::$app->controller->action->id=='changepassword'):?>
<?php ActiveForm::end(); ?>
<?php endif;?>           