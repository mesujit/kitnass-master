<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;  

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<?php if($model->isNewRecord):?>     

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput() ?>        
    <?= $form->field($model, 'phone')->textInput() ?>        
    <?= $form->field($model, 'mobile')->textInput() ?>        
    <?= $form->field($model, 'address')->textInput() ?>        
    <?= $form->field($model, 'email')->textInput() ?>  

    <?= $form->field($model, 'status')->textInput() ?>        


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php else:?>  
<div class="updateContents">          
			<div class="alert alert-success alert-dismissible chng-pass" style="display:none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                Password changed.
			</div>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Update Profile</a></li>
                  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Change Password</a></li>
                  
                 </ul>  
                
                <?php $form = ActiveForm::begin(['id'=>'change-password']); ?>     
                <div class="tab-content">	
                         
                    <div class="tab-pane active" id="tab_1">      
                        <div class="user-form">          
                
                            <?= $form->field($model, 'name')->textInput() ?>        
                            <?= $form->field($model, 'phone')->textInput() ?>        
                            <?= $form->field($model, 'mobile')->textInput() ?>        
                            <?= $form->field($model, 'address')->textInput() ?>  
                            <?= $form->field($model, 'email')->textInput() ?>
                            <?= $form->field($model, 'status')->textInput() ?>
                            
                            <?php /* $form->field($model, 'status')->dropDownList(common\components\Helper::$status) */?>
                
                            <div class="form-group">
                                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>
                
                        </div>   
                            
                    </div>
                  <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <?php echo $this->renderAjax('change-pass-form', [
                        'changePasswordModel'=>$changePasswordModel,'model'=>$model,'form'=>$form
                    ]);?>
                    </div>
                <!-- /.tab-content -->  
                 <?php ActiveForm::end(); ?>
            </div>
            </div>
        </div>       
<style>  

    .nav-tabs-custom>.nav-tabs>li:first-of-type.active>a {
        border-left-color: transparent;
    }
    .nav-tabs-custom>.nav-tabs>li.active>a {
        border-top-color: transparent;
        border-left-color: #f4f4f4;
        border-right-color: #f4f4f4;
    }
    .nav-tabs-custom>.nav-tabs>li.active>a, .nav-tabs-custom>.nav-tabs>li.active:hover>a {
        background-color: #fff;
        color: #444;
    }
    .nav-tabs-custom>.nav-tabs>li>a, .nav-tabs-custom>.nav-tabs>li>a:hover {
        background: transparent;
        margin: 0;
    }
    .nav-tabs-custom>.nav-tabs>li>a {
        color: #444;
        border-radius: 0;
    }
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
        color: #555;
        cursor: default;
        background-color: #fff;
        border: 1px solid #ddd;
        border-bottom-color: transparent;
    }
    .nav-tabs>li>a {
        margin-right: 2px;
        line-height: 1.42857143;
        border: 1px solid transparent;
        border-radius: 4px 4px 0 0;
    }
    .nav>li>a {
        position: relative;
        display: block;
        padding: 10px 15px;
    }
    .nav-tabs-custom>.nav-tabs>li:not(.active)>a:hover, .nav-tabs-custom>.nav-tabs>li:not(.active)>a:focus, .nav-tabs-custom>.nav-tabs>li:not(.active)>a:active {
        border-color: transparent;
    }
    .nav-tabs-custom>.nav-tabs>li>a:hover {
        color: #999;
    }
    .nav-tabs-custom>.nav-tabs>li>a, .nav-tabs-custom>.nav-tabs>li>a:hover {
        background: transparent;
        margin: 0;
    }
    .nav-tabs-custom>.nav-tabs>li>a {
        color: #444;
        border-radius: 0;
    }
    .nav>li>a:hover, .nav>li>a:active, .nav>li>a:focus {
        color: #444;
        background: #f7f7f7;
    }
    .nav-tabs>li>a:hover {
        border-color: #eee #eee #ddd;
    }
    .nav>li>a:focus, .nav>li>a:hover {
        text-decoration: none;
        background-color: #eee;
    }
    .nav-tabs>li>a {
        margin-right: 2px;
        line-height: 1.42857143;
        border: 1px solid transparent;
        border-radius: 4px 4px 0 0;
    }
    .nav>li>a {
        position: relative;
        display: block;
        padding: 10px 15px;
    }
    a:hover, a:active, a:focus {
        outline: none;
        text-decoration: none;
        color: #72afd2;
    }
</style>
<?php
$js =<<<JS
	$("body").on("click",'#changepassword',function(ev){   
		ev.preventDefault();  
		
		var obj = $(this);
		$.ajax({
			type:'post',
			url:obj.data('aurl'),
			data:$("body #change-password").serialize(),     
			dataType:'json',
			success:function(res){
				if(res.success==1){
					$("body #change-password")[0].reset();
					$("body .chng-pass").show().delay(5000).fadeOut();
				}else{
					$("body #tab_2").html(res.data);
				}
			}
		})
		});
JS;
$this->registerJS($js);           
?>
<?php endif;?>