<?php
use yii\widgets\Breadcrumbs;
//use yii\widgets\Alert;
use yii\helpers\Html;

?>          
<div class="kt-breadcrumb">       
    <nav class="breadcrumb">             
        <?= Breadcrumbs::widget([
            'itemTemplate' => "<li>{link}&nbsp;&nbsp;&nbsp;></li>\n", 
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>     
      </nav> 
         
</div> 
  
<div class="kt-mainpanel"> 
    <div class="kt-pagebody">        
     
        <?= $content ?>   
    </div>      
     
    <div class="kt-footer">
        <span>Copyright &copy;. All Rights Reserved. Setnepal</span>
        <!-- <span>Created by: ThemePixels, Inc.</span> -->
    </div>
</div>                      
           
<style>
.breadcrumb>li {
display: inline-block;
}
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #337ab7;
    border-color: #337ab7;
}

.pagination>li>a {
    background: #fafafa;
    color: #666;
}
.pagination>li>a, .pagination>li>span {
    position: relative;
    float: left;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid #ddd;
}
</style>

<?php   
$js = <<<JS
$(document).ready(function() {     
    $('#type_control').on('change',function(){       
        if ( this.value == 1)
      {
        $("#image_hideshow").show();
        $("#video_hideshow").hide();
      }
      if ( this.value == 2)
      {
        $("#video_hideshow").show();
        $("#image_hideshow").hide();
        
      }
    });
});
JS;
$this->registerJS($js);    
?>
            