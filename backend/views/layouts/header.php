<?php
use yii\helpers\Html;  

/* @var $this \yii\web\View */  
/* @var $content string */
?>  

<div class="kt-headpanel">                 
  <div class="kt-headpanel-left">
    <a id="naviconMenu" href="" class="kt-navicon d-none d-lg-flex"><i class="icon ion-navicon-round"></i></a>
    <a id="naviconMenuMobile" href="" class="kt-navicon d-lg-none"><i class="icon ion-navicon-round"></i></a>
  </div><!-- kt-headpanel-left -->
   
  <div class="kt-headpanel-right">
    
    <div class="dropdown dropdown-profile">       
      <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">   
        <img src="img/1.jpg" class="wd-32 rounded-circle" alt="">
        <span class="logged-name"><span class="hidden-xs-down"></span> <i class="fa fa-angle-down mg-l-3"></i></span>
      </a>
      <div class="dropdown-menu wd-200">          
        <ul class="list-unstyled user-profile-nav"> 
        <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">   
          <img src="img/1.jpg" class="wd-32 rounded-circle" alt="">
          <span class="logged-name"><span class="hidden-xs-down"></span> <i class=""></i></span>
      </a>
          <?= Html::a('Profile',['/user/update', 'id' => Yii::$app->user->id],['data-method' => 'post', 'class' => ''] ) ?> 
   
          <li><?= Html::a( 'Sign out',['/site/logout'],['data-method' => 'post', 'class' => '']) ?> </li>
        </ul>               
      </div><!-- dropdown-menu -->
    </div><!-- dropdown -->    
  </div><!-- kt-headpanel-right -->
</div>                  
                               