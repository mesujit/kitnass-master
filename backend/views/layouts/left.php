<?php 
use yii\helpers\Html;
   
?>
<div class="kt-sideleft-header">               
  <div class="kt-logo"><a href="index.php">SetNepal</a></div>       
  <div id="ktDate" class="kt-date-today"></div>       
  <div class="card bd-0">   
      <a href="" class="nav-link nav-link-profile">   
        <img src="img/1.jpg" class="wd-32 rounded-circle userpiccustom" alt="">
        <!-- <span class="logged-name"><span class="hidden-xs-down"></span> <i class="fa fa-angle-down mg-l-3"></i></span> -->
      </a>             
    <!-- </span>                   -->
  </div><!-- input-group --> 
</div><!-- kt-sideleft-header -->

<!-- ##### SIDEBAR MENU ##### -->  
<div class="kt-sideleft">  
  <label class="kt-sidebar-label">Navigation</label>                       
  <ul class="nav kt-sideleft-menu">
    <li class="nav-item">        
      <a href="index.php" class="nav-link active">    
        <i class="icon ion-ios-home-outline"></i>                         
        <span>Dashboard</span>  
      </a>
    </li><!-- nav-item -->        
    <li class="nav-item">                      
      <?= Html::a('Banner', ['/banner/index'], ['class' => 'nav-link']) ?> 
    </li><!-- nav-item -->
    <li class="nav-item">
      <?= Html::a('Category', ['/category/index'], ['class' => 'nav-link']) ?>
    </li>
    <li class="nav-item">
      <?= Html::a('Celebrity', ['/celebrity/index'], ['class' => 'nav-link']) ?>
    </li>
    <li class="nav-item">
      <!-- <?php Html::a('Celebrity gallery', ['/celebrity-gallery/index'], ['class' => 'nav-link']) ?> -->
    </li>
    <li class="nav-item">
      <?= Html::a('Event', ['/event/index'], ['class' => 'nav-link']) ?>
    </li>
    <li class="nav-item">
      <?= Html::a('Gallery', ['/gallery/index'], ['class' => 'nav-link']) ?>
    </li>
    <li class="nav-item">
      <?= Html::a('News', ['/news/index'], ['class' => 'nav-link']) ?>
    </li>
    <li class="nav-item">
      <?= Html::a('Static page', ['/static-page/index'], ['class' => 'nav-link']) ?>
    </li>
  </ul>
    
  </ul>
</div><!-- kt-sideleft -->
<style>
img.wd-32.rounded-circle.userpiccustom {
    width: 52px;
    height: auto;
}
</style>