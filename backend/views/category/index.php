<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>  
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],  

            'title',
            [
                'attribute' => 'type',
                'filter' => true,  
                'value' => function($model){
                    return $model::$type[$model->type];      
                }   
            ],   
            [
                'format' => 'raw',
                'value' => function($model){
                    return yii\helpers\Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $model->id]).yii\helpers\Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->id]);
                    
                }
            ],

        ],
    ]); ?>  
</div>
