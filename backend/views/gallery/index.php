<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\GallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Galleries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Gallery', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'title',  
            [
                'attribute' => 'image',
                'filter' => false,     
                'format' => 'html',                  
                'value' => function($model){
                    // echo Url::base();exit;
                    return  $model->type==1?Html::img(Url::base().'/uploads/gallery/'. $model->image,['width' => '60px',
                    'height' => '60px']):Html::img('https://img.youtube.com/vi/'.$model->youtube_id.'/default.jpg',['width' => '60px',
                    'height' => '60px']);           
                }   
            ],         
            [
                'attribute' => 'type',  
                'filter' => false,  
                'value' => function($model){
                    return $model::$type[$model->type];      
                }   
            ], 
            [
                'attribute' => 'youtube_id',  
                'filter' => false,  
                'value' => function($model){
                   
                    return $model->youtube_id;      
                }   
            ], 

           ['class' => 'common\components\ActionColumn'], 
        ],
    ]); ?>
</div>
