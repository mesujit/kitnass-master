<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Gallery */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'title',
            [
                'attribute' => 'image',
                'filter' => false,  
                'format' => 'html',                
                'value' => function($model){
                    // echo Url::base();exit;
                    return  Html::img(Url::base().'/uploads/gallery/'. $model->image,['width' => '60px',
                    'height' => '60px']);           
                }     
            ], 
            [
                'attribute' => 'type',
                'filter' => false,  
                'value' => function($model){
                    return $model::$type[$model->type];      
                }   
            ], 
            [
                'attribute' => 'status',
                'filter' => false,  
                'value' => function($model){
                    return $model::$status[$model->status];      
                }   
            ], 
            'created_at',
            'updated_at',
            'youtube_id',
        ],
    ]) ?>

</div>
