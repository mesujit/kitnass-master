<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;  

/* @var $this yii\web\View */      
/* @var $model common\models\Gallery */
/* @var $form yii\widgets\ActiveForm */
?>
 
<div class="gallery-form">   
  
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>  
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <div id="image_hideshow" style='display:none'>          
        <?= $form->field($model, 'image[]')->fileInput(['multiple' => 'true']) ?>        
        <?php    
            if(!empty($model->image) && !$model->isNewRecord){  
                echo Html::img(Url::base().'/uploads/event/' . $model->image, [  
                                'width' => '100px'
                            ]);    
            }?>       
    </div>

    <?= $form->field($model, 'type')->dropDownList($model::$type,['id'=>'type_control','prompt'=>"select type"]) ?>


    <div id="video_hideshow" style='display:none'>     
        <div id="video_items">
            <?= $form->field($model, 'youtube_id')->textInput(['maxlength' => true]) ?>
        </div>   
    
    <?= $form->field($model, 'status')->dropDownList($model::$status,['prompt'=>"select status"]) ?>
    
        <!-- <?=  Html::button('Add More', ['class'=>'glyphicon glyphicon-plus btn btn-default btn-sm btnadd']) ?> -->
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php             
$js = <<<JS
    $(document).ready(function(){   

        
    });     
JS;
$this->registerJs($js);  
?>
