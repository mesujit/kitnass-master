<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;  

/* @var $this yii\web\View */
/* @var $model common\models\Banner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banner-form">      

    <?php $form = ActiveForm::begin(); ?>                                     
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>   

    <div id="image_hideshow" style='display:none'>    
        <?= $form->field($model, 'image')->fileInput(['maxlength' => true]) ?>   
        <?php 
            if(!empty($model->image) && !$model->isNewRecord){
                echo Html::img(Url::base().'/uploads/banner/' . $model->image, [  
                                'width' => '100px'
                            ]);  
            }?>      
    </div>

   
    <?= $form->field($model, 'type')->dropDownList($model::$type,['id'=>'type_control','prompt'=>"select type"]) ?>
    
    <div id="video_hideshow" style='display:none'>
        <?= $form->field($model, 'youtube_id')->textInput(['maxlength' => true]) ?>
    </div>   

    <?= $form->field($model, 'status')->dropDownList($model::$status,['prompt'=>"select status"])?>

    <div class="form-group">   
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>       
    </div>  
      
    <?php ActiveForm::end(); ?>                            

</div> 
