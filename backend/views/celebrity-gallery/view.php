<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\CelebrityGallery */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Celebrity Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="celebrity-gallery-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [  
            'celebrity_id',
            [
                'attribute' => 'image',
                'filter' => false,  
                'format' => 'html',                
                'value' => function($model){
                    // echo Url::base();exit;
                    return  Html::img(Url::base().'/uploads/celebrity_gallery/'. $model->image,['width' => '60px',
                    'height' => '60px']);           
                }   
            ], 
            // 'type',
            [
                'attribute' => 'type',
                'filter' => false,  
                'value' => function($model){
                    return $model::$type[$model->type];      
                }   
            ], 
            'youtube_id',
        ],
    ]) ?>

</div>
