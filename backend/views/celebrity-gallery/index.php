<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CelebrityGallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Celebrity Galleries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="celebrity-gallery-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Celebrity Gallery', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],     

           // 'id',
            // 'celebrity_id',
            [
                'attribute' => 'image',
                'filter' => false,  
                'format' => 'html',                
                'value' => function($model){
                    // echo Url::base();exit;
                    return  Html::img(Url::base().'/uploads/celebrity_gallery/'. $model->image,['width' => '60px',
                    'height' => '60px']);           
                }   
            ], 
            // 'type',
            [
                'attribute' => 'type',
                'filter' => false,  
                'value' => function($model){
                    return $model::$type[$model->type];      
                }   
            ], 
            [
                'attribute' => 'youtube_id',
                'filter' => false,  
                'value' => function($model){
                    return $model->youtube_id;      
                }   
            ], 

           ['class' => 'common\components\ActionColumn'], 
        ],
    ]); ?>
</div>
