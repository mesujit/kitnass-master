<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;  
use common\models\Category;   
use yii\helpers\ArrayHelper;          
/* @var $this yii\web\View */
/* @var $model common\models\CelebrityGallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="celebrity-gallery-form">        

    <?php $form = ActiveForm::begin(); ?>          

    <?= $form->field($model, 'celebrity_id')->dropDownList( ArrayHelper::map(Category::find()->asArray()->all(), 'id', 'title')) ?>      

    <div id="image_hideshow" style='display:none'>        
        <?= $form->field($model, 'image')->fileInput() ?>
        <?php    
            if(!empty($model->image) && !$model->isNewRecord){
                echo Html::img(Url::base().'/uploads/celebrity_gallery/' . $model->image, [  
                                'width' => '100px'
                            ]);
            }?> 
    </div>

    <?= $form->field($model, 'type')->dropDownList($model::$type,['id'=>'type_control','prompt'=>"select type"]) ?>

    <div id="video_hideshow" style='display:none'>    
         <?= $form->field($model, 'youtube_id')->textInput(['maxlength' => true]) ?>
    </div>  

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>     

</div>       
  