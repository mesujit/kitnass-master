<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CelebrityGallery */

$this->title = 'Create Celebrity Gallery';
$this->params['breadcrumbs'][] = ['label' => 'Celebrity Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="celebrity-gallery-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
