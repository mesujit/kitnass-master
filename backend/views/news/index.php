<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create News', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],   

           // 'id',
            'title',
            [
                'attribute' => 'image',
                'filter' => false,  
                'format' => 'html',                
                'value' => function($model){
                    // echo Url::base();exit;
                    return  Html::img(Url::base().'/uploads/news/'. $model->image,['width' => '60px',
                    'height' => '60px']);           
                }   
            ], 
            // 'short_description:ntext',   
            // 'description:ntext',
            // 'slug',
            //'image',
            //'status',
            //'created_by',
            //'updated_by',
            //'created_at',
            //'updated_at',
            //'category_id',

           ['class' => 'common\components\ActionColumn'], 
        ],
    ]); ?>
</div>
