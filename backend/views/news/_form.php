<?php

use yii\helpers\Html;  
use yii\widgets\ActiveForm;
use common\models\Category;
use yii\helpers\Url;  
use yii\helpers\ArrayHelper;
use zxbodya\yii2\tinymce\TinyMce;         

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */  
?>

<div class="news-form">   

    <?php $form = ActiveForm::begin(); ?>  

    <?=$form->errorSummary($model);?>  

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>  

    <?= $form->field($model, 'short_description')->textarea(['rows' => 6]) ?>       

    <?= $form->field($model, 'description')->widget(TinyMce::className())?>  

    <?= $form->field($model, 'image')->fileInput() ?>
    <?php    
		if(!empty($model->image) && !$model->isNewRecord){
			echo Html::img(Url::base().'/uploads/news/' . $model->image, [
                            'width' => '100px'
                        ]);
        }?>   

    
    <?= $form->field($model, 'category_id')
        ->dropDownList( ArrayHelper::map(Category::find()->asArray()->all(), 'id', 'title'),
        ['prompt'=>"select category", 'multiple' => true]
        ) ?>
    
    <?= $form->field($model, 'status')->dropDownList($model::$status,['prompt'=>"select status"])?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>   

</div>
<?php             
$js = <<<JS
    $(document).ready(function(){   

        
    $('#summernote').summernote({
        height: 150
    })     
    });     
JS;
$this->registerJS($js);
?>


