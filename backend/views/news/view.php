<?php

use yii\helpers\Html;
use yii\widgets\DetailView;   
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
// echo yii::$app->user->id;exit;
?>
<div class="news-view">  

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'short_description:ntext',     
            [
                'attribute' => 'description',     
                'filter' => false,  
                'format' => 'raw',                
                'value' => function($model){
                    return  $model->description;           
                }      
            ],      
            [
                'attribute' => 'image',      
                'filter' => false,  
                'format' => 'html',                
                'value' => function($model){
                    // echo Url::base();exit;
                    return  Html::img(Url::base().'/uploads/news/'. $model->image,['width' => '60px',
                    'height' => '60px']);           
                }   
            ], 
            [
                'attribute' => 'status',
                'filter' => true,  
                'value' => function($model){
                    return $model::$status[$model->status];      
                }   
            ], 
            [
                'attribute' => 'created_by',  
                'filter' => true,  
                'value' => function($model){
                    return $model->created_by?$model->user->username:'';      
                }   
            ], 
            [
                'attribute' => 'updated_by',   
                'filter' => true,    
                'value' => function($model){
                    return $model->updated_by?$model->updatedby->username:'';      
                }   
            ], 
       
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
