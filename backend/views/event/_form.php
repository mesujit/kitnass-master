<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Category;   
use yii\helpers\ArrayHelper;          
use yii\helpers\Url;    
use zxbodya\yii2\tinymce\TinyMce;         


/* @var $this yii\web\View */  
/* @var $model common\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>  
     
<div class="event-form">                                                       
   
    <?php $form = ActiveForm::begin(); ?>                             

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'description')->widget(TinyMce::className()) ?>       

    <?= $form->field($model, 'image')->fileInput() ?>  
    <?php         
		if(!empty($model->image) && !$model->isNewRecord){
			echo Html::img(Url::base().'/uploads/event/' . $model->image, [  
                            'width' => '100px'
                        ]);
        }?>                 
   
    <?= $form->field($model, 'category_id')->dropDownList( ArrayHelper::map(Category::find()->asArray()->all(), 'id', 'title'),['prompt'=>"select category"])  ?>         
    
    <?= $form->field($model, 'event_date')->textInput(['class'=>'form-control fc-datepicker','placeholder'=>"y-m-d"]) ?>           
   
    <?= $form->field($model, 'status')->dropDownList($model::$status,['prompt'=>"select status"])?>             
    
    <div class="form-group">     
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>   

</div>   
<style>    
    

</style>
<?php         
$js = <<<JS
    $(document).ready(function(){    
        
        $('.fc-datepicker').datepicker({
            dateFormat: 'yy-m-d',
          showOtherMonths: true,
          selectOtherMonths: true
        });

    // $( ".fc-datepicker" ).datepicker({ changeYear: true,dateFormat: 'yy-mm-dd'  });                
        // $('#datepickerNoOfMonths').datepicker({
        //   showOtherMonths: true,
        //   selectOtherMonths: true,
        //   numberOfMonths: 2
        // });     
    });     
JS;
$this->registerJS($js);
?>