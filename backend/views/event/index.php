<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Event', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([     
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],                  
    
           // 'id',
            'title',  
            [
                'attribute' => 'image',     
                'filter' => false,  
                'format' => 'html',                
                'value' => function($model){
                    // echo Url::base();exit;
                    return  Html::img(Url::base().'/uploads/event/'. $model->image,['width' => '60px',
                    'height' => '60px']);           
                }   
            ],   
            // 'short_description:ntext',
            // 'description:ntext',
            // 'slug',
            //'image',
            // 'status',
            //'created_by',
            //'updated_by',
            //'created_at',
            //'updated_at',
            //'category_id',
            // 'event_date',  

           ['class' => 'common\components\ActionColumn'], 
        ],
    ]); ?>
</div>
