<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CelebritySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Celebrities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="celebrity-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Celebrity', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],   

           // 'id',
            'name',
            'about:ntext',  
            'title',
            [
                'attribute' => 'image',     
                'filter' => false,  
                'format' => 'html',                
                'value' => function($model){
                    // echo Url::base();exit;
                    return  Html::img(Url::base().'/uploads/celebrity/'. $model->image,['width' => '60px',
                    'height' => '60px']);           
                }   
            ], 
            [
                'attribute' => 'type',
                'filter' => false,  
                'value' => function($model){
                    return $model::$type[$model->type];      
                }   
            ], 

            //'filmography:ntext',
            //'awards:ntext',
            //'status',
            //'slug',
            //'image',
            //'created_at',
            //'updated_at',

           ['class' => 'common\components\ActionColumn'], 
        ],
    ]); ?>
</div>
