<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Celebrity */

$this->title = 'Update Celebrity: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Celebrities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="celebrity-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'filmography_model' => $filmography_model,
        'award_model'=>$award_model,
    ]) ?>

</div>
