<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;  
use common\models\Category;      
use yii\helpers\ArrayHelper;          
/* @var $this yii\web\View */
/* @var $model common\models\CelebrityGallery */
/* @var $form yii\widgets\ActiveForm */
// print_r($model);exit;     
?>

    <?php $form = ActiveForm::begin(['id'=>'gallery']); ?>            

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'youtube_id')->textInput(['maxlength' => true]) ?>     
        </div>
        <div class="form-group gallery_btn">
            <?= Html::submitButton('Add', ['class' => 'btn btn-success','id'=>'celebrity_gallery_form','data-aurl'=>Yii::$app->urlManager->createUrl(['celebrity/video-gallery-save','id'=>$celebrity_model->id]),'data-formid'=>'gallery_form'])?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>    

<style>
.gallery_btn{
    margin-top: 28px;
}
</style>


<?php
$js =<<<JS
	$("body").on("click",'#celebrity_gallery_form',function(ev){   
		ev.preventDefault();
		
		var obj = $(this);
        // console.log(obj);
		$.ajax({
			type:'post',
			url:obj.data('aurl'),
			data:$("body #gallery").serialize(),     
			dataType:'json',
			success:function(res){
				if(res.success==1){
                // console.log(res);
					$("body #gallery")[0].reset();
					// $.pjax.defaults.timeout = false;
				}else{
					$("body #tab_2").html(res.data);
				}
			}
		})
		});
JS;
$this->registerJS($js);           
?>