<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;  
use yii\helpers\Url;  

/* @var $this yii\web\View */
/* @var $model common\models\Celebrity */
/* @var $form yii\widgets\ActiveForm */
// print_r($award_model);exit;
?>

    <div class="celebrity-forms">     
        <?php $form = ActiveForm::begin(['id'=>'celebrity-form']); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>   

            <?= $form->field($model, 'about')->textarea(['rows' => 6]) ?>  

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'type')->dropDownList($model::$type,['prompt'=>"select type"]) ?>

            <?= $form->field($model, 'image')->fileInput(['maxlength' => true]) ?>   
            <?php    
                if(!empty($model->image) && !$model->isNewRecord){
                    echo Html::img(Url::base().'/uploads/celebrity/' . $model->image, [
                                    'width' => '100px'
                                ]);
                }?> 
    </div>
    
    <div class="award-form">   

        <div class="row">
            <div class="col-md-4">
            <h2> Awards</h2>
                <?= Html::a('Add award', '#', ['style'=>'margin-top: -30px','class'=>'pull-right btn btn-primary add-more','data-eleid'=>'award_count']) ?>
            </div>
        </div>  
        <hr />  
         
        <?php foreach ($award_model as $award_index => $award) {
            $award_count = $award_index+1;
        ?>
        <div class="each-data row" style="background:1px solid #fcfcfc">
            
            <div class="col-md-4">
                <?= $form->field($award, "[$award_index]name")->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($award, "[$award_index]release_date")->textInput(['class'=>'form-control fc-datepicker','placeholder'=>"Year of release"]) ?>
            </div>
        </div>
        <?php }?>        
        
        <input type="hidden" value="<?=$award_count;?>" name="Award[awards_count]" id="award_count" />
    </div>
    <div class="filmography-form">   

        <div class="row">
            <div class="col-md-4">
            <h2> Filmography</h2>
                <?= Html::a('Add filmography', '#', ['style'=>'margin-top: -30px','class'=>'pull-right btn btn-primary add-more','data-eleid'=>'filmography_count']) ?>
            </div>
        </div>
        <hr />  
         
        <?php foreach ($filmography_model as $filmography_index => $filmography) {
        $filmography_count = $filmography_index+1;
        ?>
        <div class="each-data row" style="background:1px solid #fcfcfc">
            
            <div class="col-md-4">
                <?= $form->field($filmography, "[$filmography_index]name")->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($filmography, "[$filmography_index]release_date")->textInput(['class'=>'form-control fc-datepicker','placeholder'=>"Year of release"]) ?>
            </div>
        </div>
        <?php }?>        
        
        <input type="hidden" value="<?=$filmography_count;?>" name="Filmography[filmographys_count]" id="filmography_count" />
    </div>


    <?= $form->field($model, 'status')->dropDownList($model::$status,['prompt'=>"select status"]) ?>
   
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

           

<style>



       
</style>
<?php         
$js = <<<JS
    $(document).ready(function(){               
    $('.fc-datepicker').datepicker({
            dateFormat: 'yy',
            changeYear: true,
          showOtherMonths: true,
          selectOtherMonths: true
        });

    $(".add-more").on('click',function(ev){
            ev.preventDefault();
            var obj = $(this);
            var ele = $("#"+obj.data("eleid"));
            count = ele.val();
            ele.val(parseInt(count) + 1);
            $("#celebrity-form").submit();
        });    


        })
JS;
$this->registerJS($js);
?> 