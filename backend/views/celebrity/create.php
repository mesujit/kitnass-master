<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Celebrity */

$this->title = 'Create Celebrity';
$this->params['breadcrumbs'][] = ['label' => 'Celebrities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="celebrity-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'filmography_model' => $filmography_model,
        'award_model'=>$award_model,
    ]) ?>

</div>
