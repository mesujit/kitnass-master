<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Celebrity */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Celebrities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
// print_r($celebrity_gallery_model);exit;
?>
<div class="celebrity-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',  
                'method' => 'post',
            ],
        ]) ?>
    </p>
         <div class="nav-tabs-custom">   
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Details</a></li>
                  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Photo gallery</a></li>
                  <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Video gallery</a></li>
                  <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Awards</a></li>
                  <li class=""><a href="#tab_5" data-toggle="tab" aria-expanded="false">Filmography</a></li>
                </ul>  
                
                 <?php $form = ActiveForm::begin(['id'=>'gallery']); ?>           
                    <div class="tab-content">	
                            
                        <div class="tab-pane active" id="tab_1">         
                            <div class="user-form">  
                                <?= DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [
                                        'name',
                                        'about:ntext',   
                                        'title',
                                  
                                        // 'status',    
                                        [
                                            'attribute' => 'status',
                                            'filter' => false,  
                                            'value' => function($model){
                                                return $model::$status[$model->status];      
                                            }   
                                        ], 
                                        [
                                            'attribute' => 'image',     
                                            'filter' => false,  
                                            'format' => 'html',                
                                            'value' => function($model){
                                                // echo Url::base();exit;
                                                return  Html::img(Url::base().'/uploads/celebrity/'. $model->image,['width' => '60px',
                                                'height' => '60px']);           
                                            }   
                                        ], 
                                        [
                                            'attribute' => 'type',
                                            'filter' => false,  
                                            'value' => function($model){
                                                return $model::$type[$model->type];      
                                            }   
                                        ], 
                                        'created_at',
                                        'updated_at',
                                    ],
                                ]) ?>
                            </div>   
                        </div>   
                        <div class="tab-pane" id="tab_3">  
                            <?php echo $this->renderAjax('celebrity_gallery_form', [
                                'model' =>$celebrity_gallery_model,
                                'dataProvider'=>$dataProvider,
                                'searchModel' => $searchModel,
                                'celebrity_model'=>$model   
                            ]);?> 
                            <?php Pjax::begin(['id' => 'gallerylist', 'timeout' => false, 'enablePushState' => false, 'clientOptions' => ['method' => 'POST']]) ?>
                                <?= GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'filterModel' => $searchModel,
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn'],     
                                            [
                                                'attribute' => 'image',
                                                'filter' => false,     
                                                'format' => 'html',                  
                                                'value' => function($model){
                                                    // echo Url::base();exit;
                                                    return  $model->type==2?Html::img('https://img.youtube.com/vi/'.$model->youtube_id.'/default.jpg',['width' => '60px',
                                                    'height' => '60px']):'';           
                                                }   
                                            ],   
                                            'name',
                                
                                            [
                                                'attribute' => 'youtube_id',
                                                'filter' => false,  
                                                'value' => function($model){
                                                    return $model->youtube_id;      
                                                }   
                                            ], 

                                        ],
                                ]); ?>  
                            <?php Pjax::end() ?>
                        </div>
                        <div class="tab-pane" id="tab_4">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>Name </h5>
                                </div>
                                <div class="col-md-6">
                                    <h5>Date </h5>
                                </div>
                            </div>
                            <div class="row">
                            <?php
                            foreach ($award as $award_key => $award_data) {?>
                                    <div class="col-md-6">
                                        <?= $award_data->name ?>
                                    </div>
                                    <div class="col-md-6">
                                         <?=$award_data->release_date  ?>
                                    </div>
                                    <?php  }?>
                                </div>
                            </div>
                        <div class="tab-pane" id="tab_5">
                            <div class="row">
                                    <div class="col-md-6">
                                        <h5>Name </h5>
                                    </div>
                                    <div class="col-md-6">
                                        <h5>Date </h5>
                                    </div>
                                </div>
                        <div class="row">
                            <?php
                            foreach ($filmography as $filmography_key => $filmography_data) {?>
                                    <div class="col-md-6">
                                        <?= $filmography_data->name ?>
                                    </div>
                                    <div class="col-md-6">
                                         <?=$filmography_data->release_date  ?>
                                    </div>
                                    <?php  }?>
                                </div>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
            </div>
</div>
<style>

    .nav-tabs-custom>.nav-tabs>li:first-of-type.active>a {
        border-left-color: transparent;
    }
    .nav-tabs-custom>.nav-tabs>li.active>a {
        border-top-color: transparent;
        border-left-color: #f4f4f4;
        border-right-color: #f4f4f4;
    }
    .nav-tabs-custom>.nav-tabs>li.active>a, .nav-tabs-custom>.nav-tabs>li.active:hover>a {
        background-color: #fff;
        color: #444;
    }
    .nav-tabs-custom>.nav-tabs>li>a, .nav-tabs-custom>.nav-tabs>li>a:hover {
        background: transparent;
        margin: 0;
    }
    .nav-tabs-custom>.nav-tabs>li>a {
        color: #444;
        border-radius: 0;
    }
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
        color: #555;
        cursor: default;
        background-color: #fff;
        border: 1px solid #ddd;
        border-bottom-color: transparent;
    }
    .nav-tabs>li>a {
        margin-right: 2px;
        line-height: 1.42857143;
        border: 1px solid transparent;
        border-radius: 4px 4px 0 0;
    }
    .nav>li>a {
        position: relative;
        display: block;
        padding: 10px 15px;
    }

    .nav-tabs-custom>.nav-tabs>li:not(.active)>a:hover, .nav-tabs-custom>.nav-tabs>li:not(.active)>a:focus, .nav-tabs-custom>.nav-tabs>li:not(.active)>a:active {
        border-color: transparent;
    }
    .nav-tabs-custom>.nav-tabs>li>a:hover {
        color: #999;
    }
    .nav-tabs-custom>.nav-tabs>li>a, .nav-tabs-custom>.nav-tabs>li>a:hover {
        background: transparent;
        margin: 0;
    }
    .nav-tabs-custom>.nav-tabs>li>a {
        color: #444;   
        border-radius: 0;
    }
    .nav>li>a:hover, .nav>li>a:active, .nav>li>a:focus {
        color: #444;
        background: #f7f7f7;
    }
    .nav-tabs>li>a:hover {
        border-color: #eee #eee #ddd;
    }
    .nav>li>a:focus, .nav>li>a:hover {
        text-decoration: none;
        background-color: #eee;
    }
    .nav-tabs>li>a {
        margin-right: 2px;
        line-height: 1.42857143;
        border: 1px solid transparent;
        border-radius: 4px 4px 0 0;
    }
    .nav>li>a {
        position: relative;
        display: block;
        padding: 10px 15px;
    }
    a:hover, a:active, a:focus {
        outline: none;
        text-decoration: none;
        color: #72afd2;
    }
</style>      