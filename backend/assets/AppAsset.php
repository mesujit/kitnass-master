<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{    
    public $basePath = '@webroot';           
    public $baseUrl = '@web';
    public $css = [     
        'css/site.css',      
        'css/katniss.css',                                 
        'css/katniss.min.css',                                   
        'lib/select2/css/select2.min.css',                                   
        // 'css/bootstrap-datepicker.min.css',                                   
        'lib/font-awesome/css/font-awesome.css',
        'lib/Ionicons/css/ionicons.css',
        'lib/perfect-scrollbar/css/perfect-scrollbar.css',    
        'lib/rickshaw/rickshaw.min.css',         
        'lib/font-awesome/css/font-awesome.css',
        'lib/Ionicons/css/ionicons.css',
        'lib/perfect-scrollbar/css/perfect-scrollbar.css',
        'lib/highlightjs/github.css',
        'lib/select2/css/select2.min.css',
        'lib/spectrum/spectrum.css',
        'lib/medium-editor/medium-editor.css',
        'lib/medium-editor/default.css',
        'lib/summernote/summernote-bs4.css',
    ];    
           
    public $js = [                
        // 'lib/jquery/jquery.js',     
        'lib/popper.js/popper.js',  
        'lib/bootstrap/bootstrap.js',
        'lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js',  
        'lib/moment/moment.js',
        'lib/jquery-ui/jquery-ui.js',
        'lib/d3/d3.js',         
        'lib/rickshaw/rickshaw.min.js',
        'http://maps.google.com/maps/api/js?key=AIzaSyAEt_DBLTknLexNbTVwbXyq2HSf2UbRBU8',
        'lib/gmaps/gmaps.js',
        'lib/chart.js/Chart.js',              
        'js/katniss.js',
        'js/ResizeSensor.js',  
        'js/dashboard.js',        
        'lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js',
        'lib/highlightjs/highlight.pack.js',
        'lib/select2/js/select2.min.js',
        'lib/spectrum/spectrum.js',
        'lib/highlightjs/highlight.pack.js',
        'lib/medium-editor/medium-editor.js',
        'lib/summernote/summernote-bs4.min.js',  
        // 'js/bootstrap-datepicker.min.js',    
        
      
    ];
             
    public $depends = [
        'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}       
            
   