<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;   
use yii\filters\AccessControl;   
use common\models\LoginForm;   
use backend\models\PasswordResetRequestForm;  
use common\models\News;
use common\models\Event;
use common\models\User;
use common\models\Celebrity;
       

       
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()     
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'request-password-reset', 'reset-password',],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'request-password-reset',
                        'reset-password',],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()  
    {
        $total_news_today = News::find()->where(['between', 'created_at', date('Y-m-d').' 00:00:00', date('Y-m-d').' 23:59:59'])->count();
        $latest_events =  Event::find()->orderBy(['id' => SORT_DESC,])->limit(5)->all();
        $total_user = User::find()->count();
        $total_Celebrity = Celebrity::find()->count();  
        
        return $this->render('index',[
            'total_news_today'=> $total_news_today,
            'latest_events'=> $latest_events,
            'total_user'=> $total_user,
            'total_Celebrity'=> $total_Celebrity
            
        ]);   
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()   
    {  
        if (!Yii::$app->user->isGuest) {  
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {   
            $model->password = '';

            return $this->render('login', [
                'model' => $model,  
            ]);
        }
    }
  
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {  
        Yii::$app->user->logout();
   
        return $this->goHome();
    }

    public function actionRequestPasswordReset()                                         
    {               
        $this->layout = 'main-login';                          
        $model = new PasswordResetRequestForm();           
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) { 
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {   
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }          
                   
        return $this->render('requestPasswordResetToken', [                  
            'model' => $model,
        ]);      
    }      
   
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)                  
    {    
        $this->layout = 'main-login';                   
        $model = \common\models\User::find()->where(['password_reset_token'=>$token])->one();
       
        $new_password = $_POST["User"]["cpassword"];
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->setNewPassword($new_password)) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();   
        }else{    
            return $this->render('resetPassword', [               
                'model' => $model,
            ]);
        }
    }
}
