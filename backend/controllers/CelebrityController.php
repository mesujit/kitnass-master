<?php

namespace backend\controllers;

use Yii;
use common\models\Celebrity;
use common\models\CelebritySearch;
use common\models\CelebrityGallery;
use common\models\CelebrityGallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\models\Filmography;
use common\models\Award;
use yii\base\Model;

/**
 * CelebrityController implements the CRUD actions for Celebrity model.
 */
class CelebrityController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Celebrity models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout= 'container';                                                                  
        $searchModel = new CelebritySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Celebrity model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)        
    {   
        $this->layout= 'container';     
        $celebrity_gallery_model = new CelebrityGallery();   
        $searchModel = new CelebrityGallerySearch();
        $dataProvider = $searchModel->searchVideoGallery($id);  
        $award = Award::find()->where(['celebrity_id'=>$id])->all();
        $filmography = Filmography::find()->where(['celebrity_id'=>$id])->all();
        // $gallery_data = CelebrityGallery::find()->where(['celebrity_id'=>$id])->all(); 
        // print_r(Yii::$app->request->post());exit;
        return $this->render('view', [
            'model' => $this->findModel($id),
            'celebrity_gallery_model' =>$celebrity_gallery_model,
            'dataProvider'=>$dataProvider,
            'award'=>$award,
            'filmography'=>$filmography,
            'searchModel' => $searchModel    
        ]);
    }

    /**
     * Creates a new Celebrity model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout= 'container';   

        $model = new Celebrity();

        $filmography_count = Yii::$app->request->post()?Yii::$app->request->post()['Filmography']['filmographys_count']:1;
        
        $award_count = Yii::$app->request->post()?Yii::$app->request->post()['Award']['awards_count']:1;
        $filmography_model = [new Filmography()];                 
        $award_model = [new Award()];  

        for($i = 1; $i < $filmography_count; $i++) {  
            $filmography_model[] = new Filmography();             
        }   
        for($i = 1; $i < $award_count; $i++) {  
            $award_model[] = new Award();               
        }  
        
        $celebrity_valid = false;
        // print_r(Yii::$app->request->post());exit;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
            $celebrity_valid = true;
        }

        $filmography_valid = false;
        if (Model::loadMultiple($filmography_model, Yii::$app->request->post()) && Model::validateMultiple($filmography_model)) {
          $filmography_valid = true; 
        }
        $award_valid = false;
        if (Model::loadMultiple($award_model, Yii::$app->request->post()) && Model::validateMultiple($award_model)) {
          $award_valid = true; 
        }
                
        if($celebrity_valid && $filmography_valid &&  $award_valid ){
             $model->saveFile();         
            $model->save(false);     
            foreach ($filmography_model as $filmography) {    
                $filmography->celebrity_id =$model->id;
                $filmography->save(false);
            }
            foreach ($award_model as $award) {  
                $award->celebrity_id =$model->id;
                $award->save(false);
            }
         
           return $this->redirect(['view', 'id' => $model->id]);         
    }

        return $this->render('create', [
            'model' => $model,
            'filmography_model' => $filmography_model,
            'award_model'=>$award_model,
        ]);
    }      

    public function actionVideoGallerySave($id){          
		if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

			$celebritygalleryModel = new CelebrityGallery();
			$celebrity_model = $this->findModel($id);  
            // $celebritygalleryModel->scenario = 'update';
			if($celebritygalleryModel->load(Yii::$app->request->post()) && $celebritygalleryModel->validate()){
                $celebritygalleryModel->celebrity_id = $id;
                $celebritygalleryModel->type = 2;
				$celebritygalleryModel->save(false);
				echo json_encode([
				'success'=>1,    
				'data'=>$this->renderAjax('celebrity_gallery_form', [
                'model'=>$celebritygalleryModel,'celebrity_model'=>$celebrity_model
                
            ])
            ]);
            
			}else{
				echo json_encode([
				'success'=>0,
				'data'=>$this->renderAjax('celebrity_gallery_form', [
                 'model'=>$celebritygalleryModel,'celebrity_model'=>$celebrity_model
				])
			]);
			}
			
				
			
			Yii::$app->end();         
			
		}
	}

    /**
     * Updates an existing Celebrity model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)   
    {       
        $this->layout= 'container';                                                                  
        $model = $this->findModel($id);
        $filmography_model = Filmography::find()->where(['celebrity_id'=>$id])->all();
        $award_model = Award::find()->where(['celebrity_id'=>$id])->all();  

        $filmography_count = Yii::$app->request->post()?(Yii::$app->request->post()['Filmography']['filmographys_count'] - count($filmography_model)):0;
        
        $award_count = Yii::$app->request->post()?(Yii::$app->request->post()['Award']['awards_count']- count($award_model)):0;


        for($i = 0; $i < $filmography_count; $i++) {  
            $filmography_model[] = new Filmography();             
        }   
        for($i = 0; $i < $award_count; $i++) {  
            $award_model[] = new Award();               
        }  
        
        $celebrity_valid = false;
        // print_r(Yii::$app->request->post());exit;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
            $celebrity_valid = true;
        }

        $filmography_valid = false;
        if (Model::loadMultiple($filmography_model, Yii::$app->request->post()) && Model::validateMultiple($filmography_model)) {
          $filmography_valid = true; 
        }
        $award_valid = false;
        if (Model::loadMultiple($award_model, Yii::$app->request->post()) && Model::validateMultiple($award_model)) {
          $award_valid = true; 
        }
                
        if($celebrity_valid && $filmography_valid &&  $award_valid ){
             $model->saveFile();         
            $model->save(false);     
            foreach ($filmography_model as $filmography) {    
                $filmography->celebrity_id =$model->id;
                $filmography->save(false);
            }
            foreach ($award_model as $award) {  
                $award->celebrity_id =$model->id;
                $award->save(false);
            }
         
           return $this->redirect(['view', 'id' => $model->id]);         
        }
   
        return $this->render('update', [      
            'model' => $model,
            'filmography_model' => $filmography_model,
            'award_model'=>$award_model,
        ]);
    }

    /**
     * Deletes an existing Celebrity model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->layout= 'container';                                                                  
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Celebrity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Celebrity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Celebrity::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
